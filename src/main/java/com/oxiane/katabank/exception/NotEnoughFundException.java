
package com.oxiane.katabank.exception;

public class NotEnoughFundException extends Exception{
    public NotEnoughFundException(long accountNumber){
        super("Account number: "+accountNumber+" has not enough funds");
    }
}
