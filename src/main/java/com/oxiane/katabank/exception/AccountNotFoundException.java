package com.oxiane.katabank.exception;


public class AccountNotFoundException extends Exception{
   public AccountNotFoundException(long accountNumber){
       super("Account number: "+accountNumber+" was not found");
   }
}
