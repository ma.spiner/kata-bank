package com.oxiane.katabank.service;

import com.oxiane.katabank.domain.Account;
import com.oxiane.katabank.exception.AccountNotFoundException;


public interface AccountService {
    Account getAccountFromAccountNumber(long accountId) throws AccountNotFoundException;
}
