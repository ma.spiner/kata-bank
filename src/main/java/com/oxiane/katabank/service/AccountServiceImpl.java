package com.oxiane.katabank.service;

import com.oxiane.katabank.domain.Account;
import com.oxiane.katabank.domain.AccountRepository;
import com.oxiane.katabank.exception.AccountNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class AccountServiceImpl implements AccountService{

    private AccountRepository accountRepository;

    public AccountServiceImpl(AccountRepository accountRepository){
        this.accountRepository=accountRepository;
    }

    @Override
    public Account getAccountFromAccountNumber(long accountNumber) throws AccountNotFoundException {
        return accountRepository.findByAccountNumber(accountNumber)
                .orElseThrow(()-> new AccountNotFoundException(accountNumber));
    }
}
