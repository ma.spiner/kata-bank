package com.oxiane.katabank.service;

import com.oxiane.katabank.domain.Account;
import com.oxiane.katabank.domain.Operation;
import com.oxiane.katabank.domain.OperationRepository;
import com.oxiane.katabank.domain.OperationRequest;
import com.oxiane.katabank.exception.AccountNotFoundException;
import com.oxiane.katabank.exception.NotEnoughFundException;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class OperationServiceImpl implements OperationService{

    private OperationRepository operationRepository;
    private AccountService accountService;
    public OperationServiceImpl(OperationRepository operationRepository, AccountService accountService){
        this.operationRepository=operationRepository;
        this.accountService=accountService;
    }

    public Operation perform(OperationRequest operationRequest)throws AccountNotFoundException,NotEnoughFundException{
        Account account = accountService.getAccountFromAccountNumber(operationRequest.getAccountNumber());
        double balance = account.getAccountPosition() + operationRequest.getValue();
        String operationType;
        if(operationRequest.getValue()>=0){
            operationType = "deposit";
        } else {
            operationType = "withdrawal";
            if(balance < 0){
                throw new NotEnoughFundException(account.getAccountNumber());
            }
        }
        Operation operation = new Operation(operationType,
                operationRequest.getValue(),
                LocalDateTime.now(),
                balance,
                account);

        account.setAccountPosition(account.getAccountPosition()+operation.getOperationValue());
        return operationRepository.save(operation);
    }

    @Override
    public List<Operation> getHistory(Long accountNumber) throws AccountNotFoundException {
        Account account = accountService.getAccountFromAccountNumber(accountNumber);
        List<Operation> operations = operationRepository.findByAccount(account);
        return operations;
    }

}
