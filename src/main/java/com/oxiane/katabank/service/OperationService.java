package com.oxiane.katabank.service;

import com.oxiane.katabank.domain.Operation;
import com.oxiane.katabank.domain.OperationRequest;
import com.oxiane.katabank.exception.AccountNotFoundException;
import com.oxiane.katabank.exception.NotEnoughFundException;

import java.util.List;


public interface OperationService {
    Operation perform(OperationRequest operationRequest) throws AccountNotFoundException, NotEnoughFundException;
    List<Operation> getHistory(Long accountNumber) throws AccountNotFoundException;
}
