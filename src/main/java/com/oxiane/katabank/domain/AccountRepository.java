package com.oxiane.katabank.domain;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface AccountRepository extends CrudRepository<Account, Long> {
Optional<Account> findByAccountNumber(long accountNumber);
}
