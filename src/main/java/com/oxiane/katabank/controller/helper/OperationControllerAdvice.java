package com.oxiane.katabank.controller.helper;

import com.oxiane.katabank.exception.AccountNotFoundException;
import com.oxiane.katabank.exception.NotEnoughFundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
@Slf4j
public class OperationControllerAdvice {

    @ExceptionHandler(AccountNotFoundException.class)
    @ResponseBody
    public ResponseEntity<String> accountNotFoundHandler(AccountNotFoundException accountNotFoundException){
        log.error("accountNotFoundHandler : " + accountNotFoundException.getMessage());
        return new ResponseEntity<>(accountNotFoundException.getMessage(),HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NotEnoughFundException.class)
    @ResponseBody
    public ResponseEntity<String> notEnoughFundHandler(NotEnoughFundException notEnoughFundException){
        log.error("notEnoughFundHandler : " + notEnoughFundException.getMessage());
        return new ResponseEntity<>(notEnoughFundException.getMessage(),HttpStatus.FORBIDDEN);
    }
}
