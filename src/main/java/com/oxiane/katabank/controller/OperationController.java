package com.oxiane.katabank.controller;

import com.oxiane.katabank.domain.Operation;
import com.oxiane.katabank.domain.OperationRequest;
import com.oxiane.katabank.exception.AccountNotFoundException;
import com.oxiane.katabank.exception.NotEnoughFundException;
import com.oxiane.katabank.exception.NotEnoughFundException;
import com.oxiane.katabank.service.OperationService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping(value = "/operations")
public class OperationController {

    private final OperationService operationService;
    public OperationController(OperationService operationService){
        this.operationService = operationService;
    }


    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Operation> performOperation(@RequestBody OperationRequest operationRequest) throws NotEnoughFundException, AccountNotFoundException {
        log.info("Performing operation on account number: "+operationRequest.getAccountNumber());
        Operation operation = operationService.perform(operationRequest);
        return ResponseEntity.status(HttpStatus.CREATED).body(operation);
    }

    @GetMapping
    public ResponseEntity<List<Operation>> getHistory(@RequestParam(name = "accountNumber")Long accountNumber) throws AccountNotFoundException{
        log.info("Getting operation history on account number: "+accountNumber);
        List<Operation> operations = operationService.getHistory(accountNumber);
        return ResponseEntity.status(HttpStatus.OK).body(operations);
    }
}
