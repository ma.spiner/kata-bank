package com.oxiane.katabank.UnitTests;


import com.oxiane.katabank.domain.Account;
import com.oxiane.katabank.domain.Operation;
import com.oxiane.katabank.domain.OperationRepository;
import com.oxiane.katabank.domain.OperationRequest;
import com.oxiane.katabank.exception.AccountNotFoundException;
import com.oxiane.katabank.exception.NotEnoughFundException;
import com.oxiane.katabank.service.AccountService;
import com.oxiane.katabank.service.OperationServiceImpl;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;

import org.mockito.Mock;

import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;
import static org.mockito.Mockito.any;

@ExtendWith(MockitoExtension.class)
public class OperationServiceTest {

    private OperationServiceImpl operationService;
    private Account account;

    @Mock
    private AccountService accountService;
    @Mock
    private OperationRepository operationRepository;


    @BeforeEach
    public void setup(){
        this.operationService = new OperationServiceImpl(operationRepository,accountService);
        this.account = new Account(1,0d);
    }

    @Test
    public void depositTest() throws AccountNotFoundException, NotEnoughFundException {
        OperationRequest operationRequest = new OperationRequest(1l,50d);
        Operation operationResponseExpected = new Operation("deposit",50d, LocalDateTime.now(),50d,this.account);
        when(operationRepository.save(any()))
                .thenReturn(operationResponseExpected);
        when(accountService.getAccountFromAccountNumber(anyLong()))
                .thenReturn(this.account);

        Operation operationReturned =  operationService.perform(operationRequest);
        assertThat(operationReturned,is(operationResponseExpected));
    }

    @Test
    public void withdrawalTest() throws AccountNotFoundException, NotEnoughFundException {
        account.setAccountPosition(500d);
        OperationRequest operationRequest = new OperationRequest(1l,-50d);
        Operation operationResponseExpected = new Operation("withdrawal",-50d, LocalDateTime.now(),-50d,account);

        when(operationRepository.save(any()))
                .thenReturn(operationResponseExpected);
        when(accountService.getAccountFromAccountNumber(anyLong()))
                .thenReturn(this.account);

        Operation operationReturned = operationService.perform(operationRequest);
        assertThat(operationReturned,is(operationResponseExpected));
    }

    @Test
    public void withdrawalWithoutEnoughFounds() throws AccountNotFoundException, NotEnoughFundException {

        assertThrows(NotEnoughFundException.class,()->{
            when(accountService.getAccountFromAccountNumber(anyLong()))
                    .thenReturn(this.account);
            account.setAccountPosition(0d);
            OperationRequest operationRequest = new OperationRequest(1l,-50d);
            Operation operationReturned =  operationService.perform(operationRequest);
        });



    }

    @Test
    public void getHistoryTest()throws AccountNotFoundException{
        Operation operationTest1 = new Operation("deposit",50d, LocalDateTime.now(),50d,account);
        Operation operationTest2 = new Operation("withdrawal",-50d, LocalDateTime.now(),0d,account);
        List<Operation> operationsExpected = Arrays.asList(operationTest1,operationTest2);
        when(operationRepository.findByAccount(any()))
                .thenReturn(operationsExpected);

        assertThat(operationService.getHistory(1l),is(operationsExpected));

    }
}
