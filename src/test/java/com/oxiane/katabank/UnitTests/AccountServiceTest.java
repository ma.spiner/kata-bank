package com.oxiane.katabank.UnitTests;

import com.oxiane.katabank.domain.Account;
import com.oxiane.katabank.domain.AccountRepository;
import com.oxiane.katabank.exception.AccountNotFoundException;
import com.oxiane.katabank.service.AccountServiceImpl;

import org.junit.jupiter.api.BeforeAll;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class AccountServiceTest {

    @Mock
    private AccountRepository accountRepository;

    private AccountServiceImpl accountService;

    @BeforeEach
    public void setup(){
        this.accountService = new AccountServiceImpl(accountRepository);
    }

    @Test
    public void getAccountFromAccountNumberTest() throws AccountNotFoundException {
        Account expectedAccount = new Account(1,0d);
        when(accountRepository.findByAccountNumber(anyLong()))
                .thenReturn(Optional.of(expectedAccount));

        Account account = accountService.getAccountFromAccountNumber(1);
        assertThat(account,is(expectedAccount));
    }

    @Test
    public void getAccountFromAccountNumberNotFoundTest() throws AccountNotFoundException{
        assertThrows(AccountNotFoundException.class,()->{
            when(accountRepository.findByAccountNumber(anyLong()))
                    .thenReturn(Optional.empty());
            accountService.getAccountFromAccountNumber(000);
        });
    }
}

